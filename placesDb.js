const fs = require('fs').promises;
const { nanoid } = require('nanoid');

const filename = './database/places.json';
const itemsfile = './database/items.json';

let data = [];
let items =[];

module.exports = {
    async init() {
        try {
            const content = await fs.readFile(filename);
            data = JSON.parse(content);
        } catch (error) {
            data = []
        }
    },
    async getAll() {
        const info = data.map(item=>({
            id : item.id,
            name: item.name
        }))
        return info
    },
    async add(item) {
        if (item.name === "") {
            return ("Please enter place name")
        } else {
            item.id = nanoid();
            data.push(item);
            await this.save();
            return (item)
        }
    },
    async getById(id) {
        return data.find(item => item.id === id)
    },
    async editById(id, editedData) {
        if (editedData.name === "") {
            return ("ERROR. Please enter a proper name for place")
        } else {
            const index = data.findIndex(item => item.id === id);
            editedData.id = id;
            data.splice(index, 1, editedData);
            await this.save();
            return editedData
        }
    },
    async deleteById(id) {
        const itemsList = await fs.readFile(itemsfile);
        items = JSON.parse(itemsList);
        const check = items.find(item => item.placeID === id)
        if (check != null) {
            return ("Cannot delete category because there are items in this place")
        } else {
            const index = data.findIndex(item => item.id === id);
            data.splice(index, 1);
            await this.save()
            return ("Deleted")
        }
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2))
    }
}