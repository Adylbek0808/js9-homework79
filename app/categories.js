const express = require('express');
const categoryDb = require('../categoryDb');

const itemsList = '../database/items.json'

const router = express.Router();

router.get('/', async (req, res) => {
    const info = await categoryDb.getAll();
    res.send(info)
});


router.get('/:id', async (req, res) => {
    const info = await categoryDb.getById(req.params.id)
    res.send(info)
})
router.put('/:id', async (req, res) => {
        const edited = req.body;
        const message = await categoryDb.editById(req.params.id, edited);
        res.send(message)
})
router.post('/', async (req, res) => {
    const message = await categoryDb.add(req.body);
    res.send(message);
})

router.delete('/:id', async (req, res) => {
    const message = await categoryDb.deleteById(req.params.id);
    res.send(message)
})

module.exports = router;