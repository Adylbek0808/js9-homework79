const express = require('express');
const placesDb = require('../placesDb');


const router = express.Router();

router.get('/', async (req, res) => {
    const info = await placesDb.getAll();
    res.send(info)
});


router.get('/:id', async (req, res) => {
    const info = await placesDb.getById(req.params.id)
    res.send(info)
})
router.put('/:id', async (req, res) => {
    const edited = req.body;
    const message = await placesDb.editById(req.params.id, edited);
    res.send(message)
})
router.post('/', async (req, res) => {
    const message = await placesDb.add(req.body);
    res.send(message);
})

router.delete('/:id', async (req, res) => {
    const message = await placesDb.deleteById(req.params.id);
    res.send(message)
})

module.exports = router;