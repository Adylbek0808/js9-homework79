const express = require('express');
const multer = require('multer');
const path = require('path')
const { nanoid } = require('nanoid')

const itemsDb = require('../itemsDb');
const config = require('../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({ storage })

router.get('/', async (req, res) => {
    const info = await itemsDb.getAll();
    res.send(info)
});

router.get('/:id', async (req, res) => {
    const info = await itemsDb.getById(req.params.id)
    res.send(info)
})

router.put('/:id', async (req, res) => {
    const edited = req.body;
    const message = await itemsDb.editById(req.params.id, edited);
    res.send(message)
})

router.post('/', upload.single('image'), async (req, res) => {

    const item = req.body
    if (req.file) {
        item.image = req.file.filename
    }
    const message = await itemsDb.add(item);
    res.send(message);
})

router.delete('/:id', async (req, res) => {
    await itemsDb.deleteById(req.params.id);
    res.send("Item deleted")
})

module.exports = router;