const fs = require('fs').promises;
const { nanoid } = require('nanoid');

const filename = './database/items.json';

let data = [];

module.exports = {
    async init() {
        try {
            const content = await fs.readFile(filename);
            data = JSON.parse(content);
        } catch (error) {
            data = []
        }
    },
    async getAll() {
        const info = data.map(item => ({
            id: item.id,
            name: item.name
        }))
        return info
    },
    async add(item) {
        if (item.name === "" || item.categoryID === "" || item.placeID === "") {
            return ("ERROR. Some fields are empty")
        } else {            
            item.id = nanoid();
            data.push(item);
            await this.save();
            return item
        }
    },

    async getById(id) {
        return data.find(item => item.id === id)
    },
    async editById(id, editedData) {
        if (editedData.name === "" || editedData.categoryID === "" || editedData.placeID === "") {
            return ("ERROR. Some fields are empty")
        } else {
            const index = data.findIndex(item => item.id === id);
            editedData.id = id;
            data.splice(index, 1, editedData);
            await this.save();
            return editedData
        }
    },
    async deleteById(id) {
        const index = data.findIndex(item => item.id === id);
        data.splice(index, 1);
        await this.save()
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2))
    }
}