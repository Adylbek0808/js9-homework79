const express = require('express');
const cors = require('cors');

const categories = require('./app/categories');
const places = require('./app/places')
const items = require('./app/items');
const categoryDb = require('./categoryDb');
const placesDb = require('./placesDb');
const itemsDb = require('./itemsDb');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());


const port = 8000;


app.use('/categories', categories);
app.use('/places', places)
app.use('/items', items)


const run = async () => {
    await categoryDb.init();
    await placesDb.init();
    await itemsDb.init();
    app.listen(port, () => {
        console.log(`Live at ${port} port`)
    });
}

run().catch(console.error)